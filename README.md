# <p align="center"> 4D-DRESS: A 4D Dataset of Real-world Human Clothing with Semantic Annotations </p>

###  <p align="center"> [Wenbo Wang*](https://wenbwa.github.io), [Hsuan-I Ho*](https://ait.ethz.ch/people/hohs), [Chen Guo](https://ait.ethz.ch/people/cheguo), [Boxiang Rong](https://ribosome-rbx.github.io), [Artur Grigorev](https://ait.ethz.ch/people/agrigorev), [Jie Song](https://ait.ethz.ch/people/song), [Juan Jose Zarate](https://ait.ethz.ch/people/jzarate), [Otmar Hilliges](https://ait.ethz.ch/people/hilliges) </p>

### <p align="center"> [CVPR 2024 (Highlight 11.9%)](https://cvpr2023.thecvf.com) </p>

## <p align="center"> [ArXiv](https://eth-ait.github.io/4d-dress/) / [Dataset](https://eth-ait.github.io/4d-dress/) / [Website](https://eth-ait.github.io/4d-dress/) </p>

<p align="center">
  <img width="100%" src="assets/4D-DRESS.png"/>
</p>
  Dataset Overview. 4D-DRESS is the first real-world 4D dataset of human clothing, capturing 64 human outfits in more than 520 motion sequences and 78k scan frames. Each motion sequence includes a) high-quality 4D textured scans; for each textured scan, we annotate b) vertex-level semantic labels, thereby obtaining c) the corresponding extracted garment meshes and fitted SMPL(-X) body meshes.
</p>

## Released
- [x] 4D-DRESS Dataset.
- [x] 4D-Human-Parsing Code.

## Env Installation
Git clone this repo:
```
git clone -b main --single-branch https://github.com/WenbWa/4D-DRESS-CVPR.git
cd 4D-DRESS-CVPR
```

Init conda environment from environment.yaml:
```
conda env create -f environment.yml
conda activate 4ddress
```
Or init conda environment via following commands:
```
conda create -n 4ddress python==3.8
conda activate 4ddress
bash env_install.sh
```

## Model Installation
Install image-based parser, [Graphonomy](https://github.com/Gaoyiminggithub/Graphonomy.git).

Download checkpoint inference.pth from [here](https://drive.google.com/file/d/1eUe18HoH05p0yFUd_sN6GXdTj82aW0m9/view?usp=sharing) and save to 4dhumanparsing/checkpoints/graphonomy/.
```
git clone https://github.com/Gaoyiminggithub/Graphonomy.git 4dhumanparsing/lib/Graphonomy
mkdir 4dhumanparsing/checkpoints
mkdir 4dhumanparsing/checkpoints/graphonomy
```

Install optical flow predictor, [RAFT](https://github.com/princeton-vl/RAFT).

Download checkpoint raft-things.pth and save to 4dhumanparsing/checkpoints/raft/models/.
```
git clone https://github.com/princeton-vl/RAFT.git 4dhumanparsing/lib/RAFT
wget -P 4dhumanparsing/checkpoints/raft/ https://dl.dropboxusercontent.com/s/4j4z58wuv8o0mfz/models.zip
unzip 4dhumanparsing/checkpoints/raft/models.zip -d 4dhumanparsing/checkpoints/raft/
```

Install segment anything model, [SAM](https://github.com/facebookresearch/segment-anything).

Download checkpoint sam_vit_h_4b8939.pth and save to 4dhumanparsing/checkpoints/sam/.
```
pip install git+https://github.com/facebookresearch/segment-anything.git
wget -P 4dhumanparsing/checkpoints/sam/ https://dl.fbaipublicfiles.com/segment_anything/sam_vit_h_4b8939.pth
```

Install and compile graph-cut optimizer, [pygco](https://github.com/yujiali/pygco.git):
```
git clone https://github.com/yujiali/pygco.git 4dhumanparsing/lib/pygco
cd 4dhumanparsing/lib/pygco
wget -N -O gco-v3.0.zip https://vision.cs.uwaterloo.ca/files/gco-v3.0.zip
unzip -o gco-v3.0.zip -d  ./gco_source
make all
cd ../../..
```

## Download Dataset
Please download the 4D-DRESS dataset and place the folders according to the following structure:

    4D-DRESS
    └── < Subject ID > (00***)
        └── < Outfit > (Inner, Outer)
           └── < Sequence ID > (Take*)
                ├── basic_info.pkl: {'scan_frames', 'rotation', 'offset', ...}
                ├── Meshes_pkl
                │   ├── atlas-fxxxxx.pkl: uv texture map as pickle file (1024, 1024, 3)
                │   └── mesh-fxxxxx.pkl: {'vertices', 'faces', 'colors', 'normals', 'uvs'}
                ├── SMPL
                │   ├── mesh-fxxxxx_smpl.pkl: SMPL params
                │   └── mesh-fxxxxx_smpl.ply: SMPL mesh
                ├── SMPLX
                │   ├── mesh-fxxxxx_smplx.pkl: SMPLX params
                │   └── mesh-fxxxxx_smplx.ply: SMPLX mesh
                ├── Semantic
                │   ├── labels
                │   │   └── label-fxxxxx.pkl, {'scan_labels': (nvt, )}
                │   ├── clothes: let user extract
                │   │   └── cloth-fxxxxx.pkl, {'upper': {'vertices', 'faces', 'colors', 'uvs', 'uv_path'}, ...}
                ├── Capture
                │   ├── cameras.pkl: {'cam_id': {"intrinsics", "extrinsics", ...}}
                │   ├── < Camera ID > (0004, 0028, 0052, 0076)
                │   │   ├── images
                │   │   │   └── capture-f*****.png: captured image (1280, 940, 3) 
                │   │   ├── masks
                │   │   │   └── mask-f*****.png: rendered mask (1280, 940)
                │   │   ├── labels: let user extract
                │   │   │   └── label-f*****.png: rendered label (1280, 940, 3) 
                └── └── └── └── overlap-f*****.png: overlapped label (1280, 940, 3)


## Postprocess Dataset
Visualize 4D-DRESS sequence within [aitviewer](https://github.com/eth-ait/aitviewer):
```
python dataset/visualize.py --subj 00122  --outfit Outer --seq Take9
```

Extract labeled cloth meshes and render multi-view pixel labels using vertex annotations:
```
python dataset/dataset/postprocess.py --subj 00122  --outfit Outer --seq Take9
```


## 4D Human Parsing on 4D-DRESS
First, run image parser, optical flow, and segment anything models on 4D scan sequence:
```
python 4dhumanparsing/multi_view_parsing.py --subj 00122  --outfit Outer --seq Take9
```
Second, run graph-cut optimization and introduce manual rectification on 4D scan sequence:
```
python 4dhumanparsing/multi_surface_parsing.py --subj 00122  --outfit Outer --seq Take9
```

## Citation
```
```

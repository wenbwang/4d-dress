
cd 4dhumanparsing/lib
git clone https://github.com/Gaoyiminggithub/Graphonomy.git
git clone https://github.com/princeton-vl/RAFT.git
pip install git+https://github.com/facebookresearch/segment-anything.git

git clone https://github.com/yujiali/pygco.git
cd pygco
wget -N -O gco-v3.0.zip https://vision.cs.uwaterloo.ca/files/gco-v3.0.zip
unzip -o gco-v3.0.zip -d  ./gco_source
make all
cd ../..


mkdir checkpoints && cd checkpoints

mkdir graphonomy && cd graphonomy
# Please download and save inference.pth here from https://drive.google.com/file/d/1eUe18HoH05p0yFUd_sN6GXdTj82aW0m9/view?usp=sharing
cd ..

mkdir raft
wget -P ./raft/ https://dl.dropboxusercontent.com/s/4j4z58wuv8o0mfz/models.zip
unzip ./raft/models.zip -d ./raft/

mkdir sam
wget -P ./sam/ https://dl.fbaipublicfiles.com/segment_anything/sam_vit_h_4b8939.pth

cd ..
import os
import tqdm
import glob
import time
import torch
import shutil
import pickle
import cv2 as cv
import numpy as np
from PIL import Image
from lib.utils.sam_utils import Sam
from lib.utils.rafter_utils import Rafter
from lib.utils.parser_utils import GraphParser

from lib.utils.image_utils import *
from lib.utils.general_utils import *


# generate image parser, optical flow, and segment masks on subj_cloth_seq data
def preprocess_subj_cloth_seq(dataset_folder, subj, cloth, seq, parser_agent, rafter_agent, sam_agent, update=False):
    # locate subj_cloth_seq_dir
    surface = cloth.lower()
    subj_cloth_seq_dir = os.path.join(dataset_folder, subj, cloth, seq)
    # locate subj_cloth_seq cameras
    camera_fns = sorted(glob.glob(os.path.join(subj_cloth_seq_dir, '00*')))
    camera_names = [fn.split('/')[-1] for fn in camera_fns]
    # locate subj_cloth_seq frames
    frame_fns = sorted(glob.glob(os.path.join(subj_cloth_seq_dir, camera_names[0], 'capture_images', '*.png')))
    frame_names = [fn.split('/')[-1].split('.')[0] for fn in frame_fns]
    frame_nums = len(frame_names)

    # process all scan_frames
    loop = tqdm.tqdm(range(frame_nums))
    for n_frame in loop:
        # locate current frame
        frame = frame_names[n_frame]
        # process all cameras
        for camera in camera_names:
            loop.set_description('## Segmenting Subj/Cloth/Seq {}/{}/{} Camera {} Image Frame: {}/{}'.format(subj, cloth, seq, camera, frame, frame_names[-1]))
            # locate parser, optical, segment, track folders
            image_dir = os.path.join(subj_cloth_seq_dir, camera, 'capture_images')
            mask_dir = os.path.join(subj_cloth_seq_dir, camera, 'capture_masks')
            parser_dir = os.path.join(subj_cloth_seq_dir, camera, 'parser_labels')
            optical_dir = os.path.join(subj_cloth_seq_dir, camera, 'optical_flows')
            segment_mask_dir = os.path.join(subj_cloth_seq_dir, camera, 'segment_masks')
            group_label_dir = os.path.join(subj_cloth_seq_dir, camera, 'group_labels')
            # make folders
            if n_frame == 0:
                os.makedirs(parser_dir, exist_ok=True)
                os.makedirs(optical_dir, exist_ok=True)
                os.makedirs(segment_mask_dir, exist_ok=True)
                os.makedirs(group_label_dir, exist_ok=True)

            # Step0: load capture_image
            capture_image = load_image(os.path.join(image_dir, '{}.png'.format(frame)))
            capture_mask = load_image(os.path.join(mask_dir, '{}.png'.format(frame)))
            capture_image[capture_mask!=255] = 255
            
            # Step1: apply image parser on original image
            parser_label_fn = os.path.join(parser_dir, '{}_parser.png'.format(frame))
            parser_group_fn = os.path.join(parser_dir, '{}.png'.format(frame))
            if update or not os.path.exists(parser_group_fn):
                # parse original capture_image(h, w, 3) to parser_image(h, w, 3) and parser_group_image(h, w)
                parser_image, parser_value = parser_agent.parser_image(capture_image)
                parser_group_image, parser_group = parser_agent.group_parser_value(parser_value, surface)
                save_image(parser_label_fn, parser_image)
                save_image(parser_group_fn, parser_group_image)
            else:
                # load parser_group_image
                parser_group_image = load_image(parser_group_fn)
            # locate parser_human_mask and human_bbox
            parser_human_mask = parser_group_image != 255
            human_bbox = locate_bbox(parser_human_mask)

            # Step2: apply optical flow on original image
            optical_flow_fn = os.path.join(optical_dir, '{}.npz'.format(frame))
            optical_flow_image_fn = os.path.join(optical_dir, '{}_view.png'.format(frame))
            if update or not os.path.exists(optical_flow_fn):
                if n_frame == 0:
                    previous_image = capture_image.copy()
                else:
                    previous_image = load_image(os.path.join(image_dir, '{}.png'.format(frame_names[n_frame - 1])))
                    previous_mask = load_image(os.path.join(mask_dir, '{}.png'.format(frame_names[n_frame - 1])))
                    previous_image[previous_mask!=255] = 255
                # obtain optical flows
                optical_flows = rafter_agent.raft_image_pairs(previous_image, capture_image)
                rafter_agent.save_optical_flows(optical_flow_fn, optical_flows)
                # draw sampled optical flows for visualization
                optical_flows_sampled = rafter_agent.draw_sampled_flow(previous_image, optical_flows, n_sample=64, mask=False)
                save_image(optical_flow_image_fn, optical_flows_sampled)

            # Step3: segment masks on capture_image
            segment_mask_fn = os.path.join(segment_mask_dir, '{}.json'.format(frame))
            segment_mask_image_fn = os.path.join(segment_mask_dir, '{}.png'.format(frame))
            if update or not os.path.exists(segment_mask_fn):
                # segment cropped images to masks
                segment_masks = sam_agent.segment_images(apply_bbox(capture_image, human_bbox))
                # save segment_masks, TODO: encode to save space
                sam_agent.save_segment_masks(segment_mask_fn, segment_masks, human_bbox)
                # draw segment_mask_image
                segment_masks, bbox = sam_agent.load_segment_masks(segment_mask_fn, tensor=True, cuda=True)
                segment_mask_image = sam_agent.draw_masks(segment_masks)
                save_image(segment_mask_image_fn, segment_mask_image)


if __name__ == "__main__":
    # set dtype and device
    DTYPE = torch.float32
    DEVICE = torch.device('cuda:0')
    # set dataset and checkpoint folder
    dataset_folder = "/mnt/ssd/data1/HOODs/Datasets"
    checkpoint_dir = os.path.join(dataset_folder, 'checkpoints')

    # init Graphonomy Parser
    parser_agent = GraphParser(model_path=os.path.join(checkpoint_dir, 'graphonomy'), init_model=True, dtype=DTYPE, device=DEVICE)
    # init Optical Flow Rafter
    rafter_agent = Rafter(model_path=os.path.join(checkpoint_dir, 'raft'), init_model=True, dtype=DTYPE, device=DEVICE)
    # init Sam Model
    sam_agent = Sam(model_path=os.path.join(checkpoint_dir, 'sam'), init_model=True, dtype=DTYPE, device=DEVICE)

    # # set subj_cloth_seq data
    # subj, cloth, seqs = '00190', 'Inner', ['Take2', 'Take3', 'Take4', 'Take5', 'Take6', 'Take7', 'Take8']
    # subj, cloth, seqs = '00190', 'Outer', ['Take10', 'Take11', 'Take13', 'Take14', 'Take15', 'Take16', 'Take17']
    # subj, cloth, seqs = '00187', 'Inner', ['Take1', 'Take2', 'Take3', 'Take4', 'Take5', 'Take6', 'Take7', 'Take8', 'Take9']
    # subj, cloth, seqs = '00174', 'Inner', ['Take2', 'Take3', 'Take4', 'Take5', 'Take6', 'Take7', 'Take8', 'Take9', 'Take10']
    # subj, cloth, seqs = '00152', 'Outer', ['Take10', 'Take12', 'Take13', 'Take15', 'Take16', 'Take17', 'Take18', 'Take19']
    # subj, cloth, seqs = '00148', 'Inner', ['Take1', 'Take2', 'Take4', 'Take5', 'Take6']
    # subj, cloth, seqs = '00148', 'Inner', ['Take7', 'Take8', 'Take9', 'Take10']
    # subj, cloth, seqs = '00001', 'Inner', ['Take1']
    # subj, cloth, seqs = '00001', 'Inner', ['Take2']
    # subj, cloth, seqs = '00008', 'Inner', ['Take1']
    # subj, cloth, seqs = '00008', 'Inner', ['Take2']
    # subj, cloth, seqs = '00007', 'Inner', ['Take1']

    subj, cloth, seqs = '00136', 'Outer', ['Take17', 'Take18', 'Take19', 'Take20', 'Take21', 'Take22', 'Take23', 'Take25', 'Take27', 'Take28']
    for seq in seqs:
        preprocess_subj_cloth_seq(dataset_folder, subj, cloth, seq, parser_agent, rafter_agent, sam_agent, update=False)
    
    
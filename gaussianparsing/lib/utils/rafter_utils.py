
import os
import argparse
from lib.utils.image_utils import *

from lib.raft.core.raft import RAFT
from lib.raft.core.utils.utils import InputPadder


class Rafter:
    """
    Optical Flow Predictor
    """
    def __init__(self, model_path='', init_model=False, dtype=torch.float32, device=torch.device('cuda:0')):

        # init dtype and device
        self.dtype = dtype
        self.device = device
        # locate checkpoint
        self.checkpoint_fn = os.path.join(model_path, 'raft-things.pth')

        # init network for optical flow only
        if init_model:
            # load network with checkpoint
            self.network = self.get_network()

    # get network
    def get_network(self):
        # init parser
        parser = argparse.ArgumentParser()
        parser.add_argument('--model', help="restore checkpoint")
        parser.add_argument('--path', help="dataset for evaluation")
        parser.add_argument('--small', action='store_true', help='use small model')
        parser.add_argument('--mixed_precision', action='store_true', help='use mixed precision')
        parser.add_argument('--alternate_corr', action='store_true', help='use efficent correlation implementation')
        # init model
        model = torch.nn.DataParallel(RAFT(parser.parse_args()))
        # load model checkpoint
        model.load_state_dict(torch.load(self.checkpoint_fn))
        # set model device and eval
        model = model.module
        model.to(self.device)
        model.eval()
        return model

    # preprocess numpy or torch image
    def preprocess_image(self, image):
        # clone tensor images
        if torch.is_tensor(image):
            return torch.tensor(image.clone(), dtype=self.dtype, device=self.device).permute(2, 0, 1).unsqueeze(0)
        # send image to torch device, convert (h, w, c) to (c, h, w)
        return torch.tensor(image, dtype=self.dtype, device=self.device).permute(2, 0, 1).unsqueeze(0)

    # save optical flow to npz
    def save_optical_flows(self, optical_fn, optical_flows):
        np.savez_compressed(optical_fn, optical_flows)
    # load optical flow from npz
    def load_optical_flows(self, optical_fn):
        return np.load(optical_fn)['arr_0']

    # raft image_pairs (h, w, c) to raft_flows (h, w, 2)
    @torch.no_grad()
    def raft_image_pairs(self, image1, image2):
        # process image pairs
        img1 = self.preprocess_image(image1)
        img2 = self.preprocess_image(image2)
        # pad input images to 8
        padder = InputPadder(img1.shape)
        img1, img2 = padder.pad(img1, img2)
        # predict flow
        flow_low, flow_up = self.network(img1, img2, iters=20, test_mode=True)
        # convert (2, x, y) to (h, w, 2)
        flow12 = flow_up[0].permute(1, 2, 0)
        flow12[..., [1, 0]] = flow12[..., [0, 1]]
        return flow12.cpu().numpy().astype(int)

    # get raft_labels (h, w, nl) from labels (h, w) and flows (h, w, 2)
    def get_raft_labels(self, labels, flows, num_classes):
        # init label dimension
        h, w = labels.shape
        # init raft_label and raft_image
        raft_labels = []
        # loop over all surface_labels
        for nl in range(num_classes):
            # init empty label
            warp_label = torch.zeros((h, w)).to(labels.device)
            # init target_labels
            target_labels = labels == nl
            # assign warp_label
            if torch.count_nonzero(target_labels) > 0:
                # get label points
                label_points = torch.stack(torch.where(target_labels)).T
                # assign warp_points
                warp_points = (label_points + flows[label_points[:, 0], label_points[:, 1]]).type(torch.long)
                warp_points[warp_points < 0] = 0
                warp_points[:, 0][warp_points[:, 0] > h - 1] = h - 1
                warp_points[:, 1][warp_points[:, 1] > w - 1] = w - 1
                # TODO: assign warp_label: one-hot
                warp_label[warp_points[:, 0], warp_points[:, 1]] = 1.
                
            # append warp_label
            raft_labels.append(warp_label)
        # return raft_labels (nl, h, w)
        return torch.stack(raft_labels, axis=0)

    # draw flow on img
    def draw_sampled_flow(self, image, flow, n_sample=32, mask=False):
        # set image dimension
        h, w, c = image.shape
        # set body mask
        body_mask = (np.sum(image, axis=-1) != 255 * 3)
        # set grid with n_sample
        image_grid = create_grid(n_sample, sx=h, sy=w).astype(int)
        # draw flow lines
        for ng in range(image_grid.shape[0]):
            # mask body region
            if mask and not body_mask[image_grid[ng, 0], image_grid[ng, 1]]: continue
            # get hwflow in xy frame
            hwflow = flow[image_grid[ng, 0], image_grid[ng, 1]]
            # draw hwflow line and point
            start_point = (image_grid[ng, 0], image_grid[ng, 1])
            end_point = (int(max(min(image_grid[ng, 0] + hwflow[0], h), 0)), int(max(min(image_grid[ng, 1] + hwflow[1], w), 0)))
            draw_line(image, start=start_point, end=end_point, color=(255, 0, 0))
            draw_point(image, start_point, radius=2, color=(0, 255, 0), thickness=1)
            draw_point(image, end_point, radius=2, color=(0, 0, 255), thickness=1)
        return image

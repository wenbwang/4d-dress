
import torch
import colorsys
import cv2 as cv
import numpy as np
import torchvision

from PIL import Image
from sklearn.decomposition import PCA

from lib.utils.general_utils import *

# mse loss 
def mse(img1, img2):
    return (((img1 - img2)) ** 2).view(img1.shape[0], -1).mean(1, keepdim=True)

# psnr loss
def psnr(img1, img2):
    mse = (((img1 - img2)) ** 2).view(img1.shape[0], -1).mean(1, keepdim=True)
    return 20 * torch.log10(1.0 / torch.sqrt(mse))

# convert features(16, H, W) to RGB(H, W, 3)
def feature_to_rgb(features):    
    # Reshape features for PCA
    H, W = features.shape[1], features.shape[2]
    features_reshaped = features.view(features.shape[0], -1).T
    # Apply PCA and get the first 3 components
    pca = PCA(n_components=3)
    pca_result = pca.fit_transform(features_reshaped.cpu().numpy())
    # Reshape back to (H, W, 3)
    pca_result = pca_result.reshape(H, W, 3)
    # Normalize to [0, 255]
    pca_normalized = 255 * (pca_result - pca_result.min()) / (pca_result.max() - pca_result.min())
    rgb_array = pca_normalized.astype('uint8')
    return rgb_array

# convert id to RGB(3)
def id_to_rgb(id, max_num_obj=256):
    if not 0 <= id <= max_num_obj:
        raise ValueError("ID should be in range(0, max_num_obj)")
    # Convert the ID into a hue value
    golden_ratio = 1.6180339887
    h = ((id * golden_ratio) % 1)           # Ensure value is between 0 and 1
    s = 0.5 + (id % 2) * 0.5       # Alternate between 0.5 and 1.0
    l = 0.5
    # Use colorsys to convert HSL to RGB
    rgb = np.zeros((3, ), dtype=np.uint8)
    if id==0:   #invalid region
        return rgb
    r, g, b = colorsys.hls_to_rgb(h, l, s)
    rgb[0], rgb[1], rgb[2] = int(r*255), int(g*255), int(b*255)
    return rgb

# visualize objects(H, W) to (H, W, 3)
def visualize_obj_color(objects, bg_value):
    rgb_mask = torch.zeros((*objects.shape[-2:], 3)).long()
    # visualize object color
    if bg_value == 255:
        all_obj_ids = torch.unique(objects)
        for id in all_obj_ids:
            rgb_mask[objects == id] = torch.tensor(id_to_rgb(id))
    # visualize label color
    else:
        for nl in range(len(SURFACE_LABEL)):
                rgb_mask[objects == nl] = torch.tensor(SURFACE_LABEL_COLOR[nl])
    return rgb_mask.cpu().numpy()

# visualize objects(H, W) to (H, W)
def visualize_obj_gray(objects, bg_value):
    gray_mask = torch.zeros((objects.shape[-2:])).long()
    # visualize object color
    if bg_value == 255:
        gray_mask = objects
    # visualize label color
    else:
        for nl in range(len(SURFACE_LABEL)):
            gray_mask[objects == nl] = torch.tensor(SURFACE_LABEL_GRAY[nl])
    return gray_mask.cpu().numpy()

# create normalized grid with n_per_side
def create_grid(n_per_side, sx=1, sy=1):
    offset = 1 / (2 * n_per_side)
    points_one_side = np.linspace(offset, 1 - offset, n_per_side)
    points_x = np.tile(points_one_side[None, :], (n_per_side, 1))
    points_y = np.tile(points_one_side[:, None], (1, n_per_side))
    points = np.stack([points_x * sx, points_y * sy], axis=-1).reshape(-1, 2)
    return points

# draw point(h, w)
def draw_point(img, point, radius=2, color=(0, 0, 0), thickness=-1):
    cv.circle(img, (int(point[1]), int(point[0])), radius=radius, color=color, thickness=thickness)

# draw text with position(h, w)
def draw_text(img, text, position, font_scale=1.5, color=(0, 0, 0), thickness=2):
    cv.putText(img, text, (int(position[1]), int(position[0])), fontFace=1, fontScale=font_scale, color=color, thickness=thickness, lineType=cv.LINE_AA)
    return img

# draw line with start(hs, ws) and end(he, we)
def draw_line(img, start, end, color=(0, 0, 0), thickness=1):
    cv.line(img, (int(start[1]), int(start[0])), (int(end[1]), int(end[0])), color=color, thickness=thickness)
    return img

# draw rectangle with start(hs, ws) and end(he, we)
def draw_rectangle(img, start, end, color=(0, 0, 0), thickness=1):
    cv.rectangle(img, (int(start[1]), int(start[0])), (int(end[1]), int(end[0])), color=color, thickness=thickness)
    return img


import os
import json
import torch
import pickle
import base64
import numpy as np
from lib.utils.image_utils import *
from pycocotools import mask as maskUtils
from segment_anything import SamPredictor, SamAutomaticMaskGenerator, sam_model_registry


class Sam:
    """
    Segment Human Images using Segment Anything.
    """

    def __init__(self, model_path='', init_model=False, dtype=torch.float32, device=torch.device('cuda:0')):

        # init dtype, device, n_views, image_size
        self.dtype = dtype
        self.device = device

        # locate sam checkpoint
        self.checkpoint_dir = os.path.join(model_path, 'sam_vit_h_4b8939.pth')

        # init model for segmenting only
        if init_model:
            # load SAM model
            self.get_networks()

    # get sam model
    def get_networks(self):
        # load sam model from checkpoint
        self.sam = sam_model_registry['vit_h'](checkpoint=self.checkpoint_dir).to(self.device)
        # load sam mask predictor
        self.mask_predictor = SamPredictor(self.sam)
        # load sam auto mask generator, with coco_rle output
        self.mask_generator = SamAutomaticMaskGenerator(self.sam, points_per_side=16, points_per_batch=64,
                                                        min_mask_region_area=100, output_mode='coco_rle')

    @torch.no_grad()
    # segment numpy images (h, w, 3) into numpy masks [nm]
    def segment_images(self, image):
        # generate masks for image
        segment_masks = sorted(self.mask_generator.generate(image), key=(lambda x: x['area']), reverse=True)
        # segment_masks:[{'segmentation', ...}, ...]
        return segment_masks
    
    @torch.no_grad()
    # segment numpy images (h, w, 3) with bbox prior
    def segment_bbox(self, image, bbox):
        # set image for mask_predictor
        self.mask_predictor.set_image(image)
        # predict masks from prompt points, return best mask
        segment_masks, scores, logits = self.mask_predictor.predict(
            box=np.array([bbox[1, 0], bbox[0, 0], bbox[1, 1], bbox[0, 1]]),  # bbox = np.array([[h_min, h_max], [w_min, w_max], [H, W]])
            multimask_output=False)
        # segment_masks:[{'segmentation', ...}, ...]
        return segment_masks

    @torch.no_grad()
    # segment numpy images (h, w, 3) with points prior
    def segment_prompt(self, image, points):
        # set image for mask_predictor
        self.mask_predictor.set_image(image)
        points[:, [0, 1]] = points[:, [1, 0]]
        # predict masks from prompt points, return best mask
        segment_masks, scores, logits = self.mask_predictor.predict(
            point_coords=points,
            point_labels=np.ones(points.shape[0]),
            multimask_output=False)
        # segment_masks:[{'segmentation', ...}, ...]
        return segment_masks

    # save segment masks into .json
    def save_segment_masks(self, mask_fn, segment_masks, bbox):
        with open(mask_fn, "w") as f:
            json.dump(segment_masks, f)
        np.save(mask_fn.replace('.json', '.npy'), bbox)

    # load segment masks from .json
    def load_segment_masks(self, mask_fn, tensor=False, cuda=False):
        # load segment_masks
        segment_masks = json.load(open(mask_fn, 'rb'))
        for mask in segment_masks:
            mask['valid'] = True
            if isinstance(mask['segmentation'], dict):
                mask['segmentation'] = maskUtils.decode(mask['segmentation'])
                if tensor: 
                    mask['segmentation'] = torch.tensor(mask['segmentation'] * 1)
                    if cuda: mask['segmentation'] = mask['segmentation'].cuda()
        bbox = np.load(mask_fn.replace('.json', '.npy'))
        return segment_masks, bbox
    

    # filter segment masks with valid mask
    def detect_valid_masks(self, masks, target_mask):
        # detect valid_mask from target_mask throuh IOU
        IOU_list = []
        for mask in masks:
            # compute IOU
            intersection = torch.logical_and(mask['segmentation'] == 1, target_mask)
            union = torch.logical_or(mask['segmentation'] == 1, target_mask)
            IOU_list.append(torch.sum(intersection) / (torch.sum(union) + 1))
        valid_mask = masks[torch.argmax(torch.asarray(IOU_list))]

        # filter masks outside target_mask
        valid_masks = []
        for mask in masks:
            # compute overlap_ratio
            intersection = torch.logical_and(mask['segmentation'] == 1, valid_mask['segmentation'] == 1)
            overlap_ratio = torch.sum(intersection) / mask['area']
            # filter mask
            if overlap_ratio > 0.8: valid_masks.append(mask)
        return valid_masks

    # restore bbox masks within img
    def restore_bbox_masks(self, masks, bbox):
        # process all masks
        for mask in masks:
            # init mask_org
            mask_org = torch.zeros((bbox[2, 0], bbox[2, 1])).to(mask['segmentation'].device)
            mask_org[bbox[0, 0]:bbox[0, 1], bbox[1, 0]:bbox[1, 1]] = mask['segmentation']
            mask['segmentation'] = mask_org
        return masks

    # draw masks
    def draw_masks(self, masks):
        # init img_masks
        img_masks = torch.ones((masks[0]['segmentation'].shape[0], masks[0]['segmentation'].shape[1], 4))
        img_masks[:, :, 3] = 0
        # paint mask to img
        for mask in masks:
            img_masks[mask['segmentation'] == 1] = torch.cat([torch.rand(3), torch.tensor([0.6])])
        return (img_masks * 255).cpu().numpy().astype(np.uint8)

    # view masked images within render image
    def view_masked_images(self, image, masks):
        # group masks to three lines
        h, w, c = image.shape
        hg, wg = 3, ((len(masks)) // 3 + 1)
        group_masks = np.ones((h * hg, w * wg, 3)) * 255.
        # process all masks
        for nm in range(len(masks)):
            # copy image
            temp_image = image.copy()
            # decode segmentation to numpy array [0, 1]
            mask_segmentation = masks[nm]['segmentation'] if isinstance(masks[nm]['segmentation'], np.ndarray) else masks[nm]['segmentation'].cpu().numpy()
            # draw mask on image
            temp_image[mask_segmentation == 0] = 255
            # draw indicators
            color = (0, 0, 255) if 'valid' in masks[nm] and masks[nm]['valid'] else (255, 0, 0)
            draw_rectangle(temp_image, (1, 1), (h - 1, w - 1), color=(0, 0, 0))
            draw_text(temp_image, 'm:{}'.format(nm), (int(h * 0.05), int(w * 0.75)), color=color)
            draw_text(temp_image, 'a:{}'.format(masks[nm]['area']), (int(h * 0.10), int(w * 0.75)), color=color)
            draw_text(temp_image, 'q:{:.3f}'.format(masks[nm]['predicted_iou']), (int(h * 0.15), int(w * 0.75)), color=color)
            # append masked image to group
            group_masks[h * (nm // wg):h * (nm // wg + 1), w * (nm % wg):w * (nm % wg + 1)] = temp_image
            # print(nm, masks[nm]['area'], masks[nm]['predicted_iou'])
        return group_masks.astype(np.uint8)

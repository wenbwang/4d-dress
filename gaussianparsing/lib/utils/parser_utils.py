from lib.utils.general_utils import *

from collections import OrderedDict
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torchvision.transforms as transforms

from lib.graphonomy.networks.graph import *
from lib.graphonomy.networks.deeplab_xception_transfer import *
from lib.graphonomy.dataloaders import custom_transforms


class GraphParser:
    """
    Human Image Parser: Graphonomy
    """
    def __init__(self, model_path='', n_classes=20, init_model=False, scale_list=None, dtype=torch.float32, device=torch.device('cuda:0')):

        # init dtype, device
        self.dtype = dtype
        self.device = device
        self.n_classes = n_classes
        self.scale_list = [1, 0.75, 0.5, 0.25] if scale_list is None else scale_list

        # locate checkpoint
        self.checkpoint_fn = os.path.join(model_path, 'inference.pth')

        # init network for parsing only
        if init_model:
            # init network and transform
            self.network = self.get_network(n_classes=self.n_classes)
            self.transform, self.transform_flip = self.get_transform()
            # init adj matrices
            self.adj1_test = Variable(torch.from_numpy(preprocess_adj(cihp_graph)).float()).unsqueeze(0).unsqueeze(0).expand(1, 1, 20, 20).to(self.device)
            self.adj2_test = torch.from_numpy(cihp2pascal_nlp_adj).float().unsqueeze(0).unsqueeze(0).expand(1, 1, 7, 20).transpose(2, 3).to(self.device)
            self.adj3_test = Variable(torch.from_numpy(preprocess_adj(pascal_graph)).float()).unsqueeze(0).unsqueeze(0).expand(1, 1, 7, 7).to(self.device)

        # assign graphonomy label colors
        self.LABEL_VAL_COLORS = torch.tensor([(0, 0, 0), (128, 0, 0), (255, 0, 0), (0, 85, 0), (170, 0, 51),  # background, hat, hair, glove, sunglasses
                                              (255, 85, 0), (0, 0, 85), (0, 119, 221), (85, 85, 0), (0, 85, 85),  # upper-cloth, dress, coat, socks, pants
                                              (85, 51, 0), (52, 86, 128), (0, 128, 0), (0, 0, 255), (51, 170, 221),  # torso-skin, scarf, skirt, face, left-arm
                                              (0, 255, 255), (85, 255, 170), (170, 255, 85), (255, 255, 0), (255, 170, 0)])  # right-arm, left-leg, right-leg, left-shoe, right-shoe,
        # assign coarse label groups: background 0, skin 1, upper 2, lower 3
        self.LABEL_GROUP_COARSE = torch.tensor([0, 1, 1, 1, 1,
                                                2, 1, 2, 1, 3,
                                                1, 1, 3, 1, 1,
                                                1, 1, 1, 1, 1])
        # assign detail label groups: background 0, skin 1, upper 2, lower 3, hair 4, glove 5, shoe 6
        self.LABEL_GROUP_INNER = torch.tensor([0, 4, 4, 5, 4,
                                               2, 2, 2, 6, 3,
                                               1, 2, 3, 1, 1,
                                               1, 1, 1, 6, 6])
        # assign multilayer label groups: background 0, skin 1, upper 2, lower 3, hair 4, glove 5, shoe 6, outer 7
        self.LABEL_GROUP_OUTER = torch.tensor([0, 4, 4, 5, 4,
                                               2, 2, 7, 6, 3,
                                               1, 2, 3, 1, 1,
                                               1, 1, 1, 6, 6])

        # assign group colors: background 0, skin 1, upper 2, lower 3, hair 4, glove 5, shoe 6, outer 7
        self.GROUP_COLORS = torch.tensor([[255, 255, 255], [128, 128, 128], [180, 50, 50], [50, 180, 50],
                                          [255, 128, 0], [128, 128, 0], [128, 0, 255], [0, 128, 255]])        
        self.GROUP_GRAYS = torch.tensor([255, 128, 98, 158, 188, 218, 38, 68])

        # assign group labels
        self.GROUP_LABELS = torch.tensor([0, 1, 2, 3, 4, 5, 6, 7])

    # get Graphonomy network
    def get_network(self, n_classes=20):
        # load network
        net = deeplab_xception_transfer_projection_savemem(n_classes=n_classes, hidden_layers=128, source_classes=7,)
        # load checkpoint
        net.load_source_model(torch.load(self.checkpoint_fn))
        # send net to device
        net.to(self.device)
        return net

    # get image data transform
    def get_transform(self):
        # init transform list
        transform, transform_flip = [], []
        # get transform for all scales: scale, normalize, flip, to_tensor
        for scale in self.scale_list:
            transform.append(transforms.Compose([custom_transforms.Scale_only_img(scale),
                                                 custom_transforms.Normalize_xception_tf_only_img(),
                                                 custom_transforms.ToTensor_only_img()]))
            transform_flip.append(transforms.Compose([custom_transforms.Scale_only_img(scale),
                                                      custom_transforms.HorizontalFlip_only_img(),
                                                      custom_transforms.Normalize_xception_tf_only_img(),
                                                      custom_transforms.ToTensor_only_img()]))
        return transform, transform_flip


    @torch.no_grad()
    # parser numpy images [nv, h, w, 3][0, 255][RGB] into numpy parser_vals [nv, h, w]
    def parser_image(self, img):
        # get dimension of image: h, w, c
        h, w, c = img.shape
        # parser images
        # convert RGB image to Image
        image = Image.fromarray((img).astype(np.uint8))
        # transform image for each scale transform
        image_list = [self.transform[ns]({'image': image, 'label': 0}) for ns in range(len(self.scale_list))]
        image_flip_list = [self.transform_flip[ns]({'image': image, 'label': 0}) for ns in range(len(self.scale_list))]
        # evaluate network
        self.network.eval()
        # predict all scaled image data
        for ns, sample_batched in enumerate(zip(image_list, image_flip_list)):
            # unpack sample
            inputs, labels = sample_batched[0]['image'], sample_batched[0]['label']
            inputs_f, _ = sample_batched[1]['image'], sample_batched[1]['label']
            inputs = torch.cat((inputs.unsqueeze(0), inputs_f.unsqueeze(0)), dim=0)
            # Forward pass of the mini-batch
            inputs = Variable(inputs, requires_grad=False).to(self.device)
            with torch.no_grad():
                outputs = self.network.forward(inputs, self.adj1_test, self.adj3_test, self.adj2_test)
                outputs = (outputs[0] + self.flip(self.flip_cihp(outputs[1]), dim=-1)) / 2
                outputs = outputs.unsqueeze(0)
                # append scaled predictions
                if ns > 0:
                    outputs = F.upsample(outputs, size=(h, w), mode='bilinear', align_corners=True)
                    outputs_final = outputs_final + outputs
                else:
                    outputs_final = outputs.clone()
        # get final prediction labels
        parser_value = torch.max(outputs_final, 1)[1][0, ...]
        parser_image = self.LABEL_VAL_COLORS[parser_value].cpu().numpy().astype(np.uint8)
        # show_image(parser_images[-1])
        return parser_image, parser_value

    @torch.no_grad()
    # group parser value[h, w] into parser group[h, w]
    def group_parser_value(self, value, surface):
        # set LABEL_GROUPS
        if surface.lower() == 'inner':
            LABEL_GROUPS = self.LABEL_GROUP_INNER.to(value.device)
        elif surface.lower() == 'outer':
            LABEL_GROUPS = self.LABEL_GROUP_OUTER.to(value.device)
        GROUP_GRAYS = self.GROUP_GRAYS.to(value.device)

        # init parser group[h, w]
        parser_group_value = torch.zeros((value.shape[0], value.shape[1]), dtype=torch.long).to(value.device)
        # group parser values 
        for nc in range(LABEL_GROUPS.shape[0]):
            parser_group_value[value==nc] = LABEL_GROUPS[nc].to(value.device)
        # convert glove to skin
        parser_group_value[parser_group_value == 5] = 1
        # convert group value to image
        parser_group_image = GROUP_GRAYS[parser_group_value].cpu().numpy().astype(np.uint8)
        return parser_group_image, parser_group_value

    def flip(self, x, dim):
        indices = [slice(None)] * x.dim()
        indices[dim] = torch.arange(x.size(dim) - 1, -1, -1, dtype=torch.long, device=x.device)
        return x[tuple(indices)]

    def flip_cihp(self, tail_list):
        # tail_list = tail_list[0]
        tail_list_rev = [None] * 20
        for xx in range(14):
            tail_list_rev[xx] = tail_list[xx].unsqueeze(0)
        tail_list_rev[14] = tail_list[15].unsqueeze(0)
        tail_list_rev[15] = tail_list[14].unsqueeze(0)
        tail_list_rev[16] = tail_list[17].unsqueeze(0)
        tail_list_rev[17] = tail_list[16].unsqueeze(0)
        tail_list_rev[18] = tail_list[19].unsqueeze(0)
        tail_list_rev[19] = tail_list[18].unsqueeze(0)
        return torch.cat(tail_list_rev, dim=0)
